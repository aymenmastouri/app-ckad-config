To configure a Kubernetes Secret to provide the necessary credentials to access a private GitLab Container Registry, you can follow these steps:

---
{
  "auths": {
    "registry.gitlab.com": {
      "auth": "BASE64_ENCODED_USERNAME_AND_PASSWORD"
    }
  }
}

---
Replace BASE64_ENCODED_USERNAME_AND_PASSWORD with your GitLab username and password encoded in Base64. You can encode your credentials using the following command:

echo -n 'USERNAME:PASSWORD' | base64

Replace USERNAME and PASSWORD with your GitLab username and password.

Create a Kubernetes Secret to store the config.json file. You can use the kubectl create secret generic command to create the Secret. Replace SECRET_NAME with a name for your Secret.

kubectl create secret generic container-registry-access --from-file=.dockerconfigjson=config.json --type=kubernetes.io/dockerconfigjson



This command creates a generic Secret named SECRET_NAME and stores the config.json file as a key in the Secret with the name .dockerconfigjson. The --type flag specifies the type of the Secret as kubernetes.io/dockerconfigjson.

Update your Deployment resource to use the Kubernetes Secret as an image pull secret. You can add a imagePullSecrets field to the spec.template.spec section of your Deployment YAML file, like this:

apiVersion: apps/v1
kind: Deployment
metadata:
  name: product-composite-service
spec:
  selector:
    matchLabels:
      app: product-composite-service
  replicas: 1
  template:
    metadata:
      labels:
        app: product-composite-service
    spec:
      imagePullSecrets:
        - name: SECRET_NAME
      containers:
        - name: product-composite-service
          image: registry.gitlab.com/itmaspro/product-composite-service:latest
          ports:
            - containerPort: 8080
              protocol: TCP
Replace SECRET_NAME with the name of the Secret you created in step 2.

Apply the updated Deployment YAML file to your Kubernetes cluster using the kubectl apply command.

kubectl apply -f deployment.yaml
This will update your Deployment resource to use the image pull secret to authenticate with your GitLab Container Registry.

That's it! Your Kubernetes cluster should now be able to access your private GitLab Container Registry using the credentials stored in the Secret you created.

#### Commands

```bash
# install ArgoCD in k8s
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml

# access ArgoCD UI
kubectl get svc -n argocd
kubectl port-forward svc/argocd-server 8080:443 -n argocd

# login with admin user and below token (as in documentation):
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 --decode && echo

# you can change and delete init password

```
</br>

#### Links

* Config repo: [https://gitlab.com/nanuchi/argocd-app-config](https://gitlab.com/nanuchi/argocd-app-config)

* Docker repo: [https://hub.docker.com/repository/docker/nanajanashia/argocd-app](https://hub.docker.com/repository/docker/nanajanashia/argocd-app)

* Install ArgoCD: [https://argo-cd.readthedocs.io/en/stable/getting_started/#1-install-argo-cd](https://argo-cd.readthedocs.io/en/stable/getting_started/#1-install-argo-cd)

* Login to ArgoCD: [https://argo-cd.readthedocs.io/en/stable/getting_started/#4-login-using-the-cli](https://argo-cd.readthedocs.io/en/stable/getting_started/#4-login-using-the-cli)

* ArgoCD Configuration: [https://argo-cd.readthedocs.io/en/stable/operator-manual/declarative-setup/](https://argo-cd.readthedocs.io/en/stable/operator-manual/declarative-setup/)
